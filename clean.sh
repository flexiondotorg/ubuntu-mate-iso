#!/usr/bin/env bash

echo "Removing transient data..."
echo " - auto/iso"
rm auto/*
echo " - *.iso"
rm *.iso
echo " - *.squashfs"
rm *.squashfs
echo " - binary"
rm -rf binary
echo " - release"
rm -rf release
echo " - pre-release"
rm -rf pre-release
echo " - chroot"
rm -rf chroot
echo " - cache"
rm -rf cache
