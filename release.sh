#!/usr/bin/env bash

if [ ! -f cycle.txt ]; then
    echo "ERROR! No cycle.txt."
    exit 1
fi

ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)
PROJECT="ubuntu-mate"
VERSION=`cat cycle.txt`
CYCLE=`cat cycle.txt | cut -d'.' -f2`
HOUR=$(date +%H)
DAY=$(date +%j)
MONTH=`date +%m`

if [ "${VERSION}" == "12.04.5" ]; then
    CODENAME="precise"
    FULLNAME="Precise Pangolin"
    REL_URL="https://ubuntu-mate.org/blog/ubuntu-mate-precise-final-release"
    QUALITY="LTS"
elif [ "${VERSION}" == "14.04.2" ]; then
    CODENAME="trusty"
    FULLNAME="Trusty Tahr"
    REL_URL="https://ubuntu-mate.org/blog/ubuntu-mate-trusty-14.04.2-release"
    QUALITY="LTS"
elif [ "${VERSION}" == "14.10" ]; then
    CODENAME="utopic"
    FULLNAME="Utopic Unicorn"
    REL_URL="https://ubuntu-mate.org/blog/ubuntu-mate-utopic-final-release/"
    QUALITY=""
elif [ "${VERSION}" == "15.04" ]; then
    CODENAME="vivid"
    FULLNAME="Vivid Vervet"
    REL_URL="https://ubuntu-mate.org/blog/ubuntu-mate-vivid-alpha2/"
    QUALITY="Alpha2"
else
    echo "ERROR! Unknown invocation."
fi

if [ "$(basename ${0})" == "final.sh" ]; then
    MODE=${CODENAME}
    PAGE=${CODENAME}
    FILEBASE="${PROJECT}-${VERSION}-desktop-${ARCH}"
    VOLID=$(echo "MATE_${VERSION}" | sed -e 's/-//g' -e 's/\.//g' | tr '[:lower:]' '[:upper:]')
    RSYNC_OPTS=""
    echo "WARNING! You have requested a FINAL RELEASE. Are you sure your want to do this?"
    echo "Press RETURN to continue or CTRL-C to cancel."
    read
elif [ "$(basename ${0})" == "release.sh" ]; then
    MODE=${CODENAME}
    PAGE=${CODENAME}
    FILEBASE="${PROJECT}-${VERSION}-${QUALITY}-desktop-${ARCH}"
    VOLID=$(echo "MATE_${VERSION}" | sed -e 's/-//g' -e 's/\.//g' | tr '[:lower:]' '[:upper:]')
    RSYNC_OPTS=""
    echo "WARNING! You have requested a RELEASE. Are you sure your want to do this?"
    echo "Press RETURN to continue or CTRL-C to cancel."
    read
else
    MODE="pre-release"
    PAGE="pre-release"
    QUALITY="dev.${DAY}${HOUR}"
    FILEBASE="${PROJECT}-${VERSION}-${QUALITY}-desktop-${ARCH}"
    VOLID=$(echo "MATE_${VERSION}" | sed -e 's/-//g' -e 's/\.//g' | tr '[:lower:]' '[:upper:]')
    RSYNC_OPTS="--delete-after"
fi

if [ ! -L binary/ubuntu ]; then
    echo "Creating 'ubuntu' symlink."
    cd binary
    ln -s . ubuntu
    cd ..
fi

# Remove live-build cruft
if [ -f binary/MD5SUMS ]; then
    rm binary/MD5SUMS
fi

# Graft ship-live package archive and the Ubuntu MATE boot configuration
cp -r _config/includes.binary.${ARCH}.${CODENAME}/* binary/

# Correct casper-uuid file name
mv -v binary/.disk/casper-uuid-*-generic binary/.disk/casper-uuid-generic
# Inject the release notes URL and project name.
echo "${REL_URL}" > binary/.disk/release_notes_url
echo "full_cd/single" > binary/.disk/cd_type
touch binary/.disk/base_installable
if [ "${QUALITY}" != "" ]; then
    echo "Ubuntu MATE ${VERSION} \"${FULLNAME}\" - ${QUALITY} ${ARCH} ($(date +%Y%m%d))" > binary/.disk/info
else
    echo "Ubuntu MATE ${VERSION} \"${FULLNAME}\" - ${ARCH} ($(date +%Y%m%d))" > binary/.disk/info
fi
cat << README > binary/README.diskdefines
#define DISKNAME  Ubuntu MATE ${VERSION} "${FULLNAME}" - ${QUALITY} ${ARCH}
#define TYPE  binary
#define TYPEbinary  1
#define ARCH  ${ARCH}
#define ARCH${ARCH}  1
#define DISKNUM  1
#define DISKNUM1  1
#define TOTALNUM  0
#define TOTALNUM0  1
README

# Mimic how official Ubuntu hash creation.
if [ -f binary/md5sum.txt ]; then
    echo "Creating md5sum.txt"
    rm binary/md5sum.txt
fi

# Remove wubi, not supported by Ubuntu MATE.
rm -f binary/wubi.exe 2>/dev/null
rm -f binary/autorun.inf 2>/dev/null

cd binary
for HASHABLE in pics preseed pool .disk EFI boot casper README.diskdefines dists install; do
    if [ -d ${HASHABLE} ]; then
        echo "Hashing directory : ${HASHABLE}"
        find ./${HASHABLE} -type f -print0 | xargs -0 md5sum >> md5sum.txt
    elif [ -f ${HASHABLE} ]; then
        echo "Hashing file      : ${HASHABLE}"
        md5sum ./${HASHABLE} >> md5sum.txt
    else
        echo "Skipping ${HASHABLE}"
    fi
done
cd ..

# Fake the time/date.
FAKEDT="$(date +%Y%m%d)1337.00"
find binary/ -exec touch -t ${FAKEDT} {} \;

echo "Making a ${MODE} of ${FILEBASE}"

rm -rfv "${MODE}"
mkdir -p "${MODE}/${ARCH}"

if [ "${ARCH}" == "amd64" ]; then
    rm -f livecd.ubuntu-mate.iso
    xorriso \
      -as mkisofs \
      -r \
      -checksum_algorithm_iso md5,sha1 \
      -isohybrid-mbr /usr/lib/syslinux/isohdpfx.bin \
      -cache-inodes \
      -J \
      -l \
      -b isolinux/isolinux.bin \
      -c isolinux/boot.cat \
      -no-emul-boot \
      -boot-load-size 4 \
      -boot-info-table \
      -eltorito-alt-boot \
      -e boot/grub/efi.img \
      -no-emul-boot \
      -isohybrid-gpt-basdat \
      -isohybrid-apm-hfsplus \
      -volid "${VOLID}" \
      -o ${MODE}/${ARCH}/${FILEBASE}.iso binary

    # Should work for Mac - obsolete since Ubuntu 14.10
    if [ "${CODENAME}" == "trusty" ]; then
        mkdir -p "${MODE}/${ARCH}+mac"
        xorriso \
        -as mkisofs \
        -r \
        -checksum_algorithm_iso md5,sha1 \
        -isohybrid-mbr /usr/lib/syslinux/isohdpfx.bin \
        -partition_offset 16 \
        -cache-inodes \
        -J \
        -l \
        -b isolinux/isolinux.bin \
        -c isolinux/boot.cat \
        -no-emul-boot \
        -boot-load-size 4 \
        -boot-info-table \
        -volid "${VOLID}" \
        -o ${MODE}/${ARCH}+mac/${FILEBASE}+mac.iso binary
    fi
elif [ "${ARCH}" == "i386" ]; then
    rm -f livecd.ubuntu-mate.iso
    xorriso \
      -as mkisofs \
      -r \
      -checksum_algorithm_iso md5,sha1 \
      -isohybrid-mbr /usr/lib/syslinux/isohdpfx.bin \
      -partition_offset 16 \
      -cache-inodes \
      -J \
      -l \
      -b isolinux/isolinux.bin \
      -c isolinux/boot.cat \
      -no-emul-boot \
      -boot-load-size 4 \
      -boot-info-table \
      -volid "${VOLID}" \
      -o ${MODE}/${ARCH}/${FILEBASE}.iso binary
else
    echo "Copying livecd.ubuntu-mate.iso"
    cp -v livecd.ubuntu-mate.iso ${MODE}/${ARCH}/${FILEBASE}.iso
fi

echo "Generating hashes"
md5sum ${MODE}/${ARCH}/${FILEBASE}.iso > ${MODE}/${ARCH}/${FILEBASE}.iso.md5
if [ "${CODENAME}" == "trusty" ]; then
    if [ "${ARCH}" == "amd64" ]; then
        md5sum ${MODE}/${ARCH}+mac/${FILEBASE}+mac.iso > ${MODE}/${ARCH}+mac/${FILEBASE}+mac.iso.md5
    fi
fi

echo "Generating torrents"
mktorrent -a udp://tracker.publicbt.com:80,udp://tracker.istole.it:80,udp://tracker.openbittorrent.com:80/announce \
          -c "Ubuntu MATE ${FILEBASE}.iso" \
          -n ${FILEBASE}.iso \
          -o ${MODE}/${ARCH}/${FILEBASE}.iso.torrent \
          -w https://ubuntu-mate.r.worldssl.net/${PAGE}/${FILEBASE}.iso,https://ubuntu-mate.org/${PAGE}/${FILEBASE}.iso,http://pub.mate-desktop.org/iso/ubuntu-mate/${MODE}/${ARCH}/${FILEBASE}.iso,http://master.dl.sourceforge.net/project/ubuntu-mate/${VERSION}/${ARCH}/${FILEBASE}.iso \
          ${MODE}/${ARCH}/${FILEBASE}.iso

if [ "${CODENAME}" == "trusty" ]; then
    if [ "${ARCH}" == "amd64" ]; then
        mktorrent -a udp://tracker.publicbt.com:80,udp://tracker.istole.it:80,udp://tracker.openbittorrent.com:80/announce \
           -c "Ubuntu MATE ${FILEBASE}+mac.iso" \
           -n ${FILEBASE}+mac.iso \
           -o ${MODE}/${ARCH}+mac/${FILEBASE}+mac.iso.torrent \
           -w https://ubuntu-mate.r.worldssl.net/${PAGE}/${FILEBASE}+mac.iso,https://ubuntu-mate.org/${PAGE}/${FILEBASE}+mac.iso,http://pub.mate-desktop.org/iso/ubuntu-mate/${MODE}/${ARCH}+mac/${FILEBASE}+mac.iso \
           ${MODE}/${ARCH}+mac/${FILEBASE}+mac.iso
    fi
fi

DEST=$(echo "bWFydGluQHdpbXByZXNzLmlvCg==" | base64 --decode)
rsync -av -e 'ssh -c arcfour128' ${RSYNC_OPTS} --progress ${MODE}/${ARCH}/ ${DEST}:ISO/${MODE}/${ARCH}/
if [ "${CODENAME}" == "trusty" ]; then
    if [ "${ARCH}" == "amd64" ]; then
        rsync -av -e 'ssh -c arcfour128' ${RSYNC_OPTS} --progress ${MODE}/${ARCH}+mac/ ${DEST}:ISO/${MODE}/${ARCH}+mac/
    fi
fi

DEST=$(echo "bWFydGluQG1hdGUtZGVza3RvcC5vcmcK" | base64 --decode)
rsync -av -e 'ssh -c arcfour128' ${RSYNC_OPTS} --progress ${MODE}/${ARCH}/ ${DEST}:/var/www/pub/iso/ubuntu-mate/${MODE}/${ARCH}/

if [ "${CODENAME}" == "trusty" ]; then
    if [ "${ARCH}" == "amd64" ]; then
        rsync -av -e 'ssh -c arcfour128' ${RSYNC_OPTS} --progress ${MODE}/${ARCH}+mac/ ${DEST}:/var/www/pub/iso/ubuntu-mate/${MODE}/${ARCH}+mac/
    fi
fi

if [ "$(basename ${0})" != "pre-release.sh" ]; then
    DEST=$(echo "cmVtaXhfODgsdWJ1bnR1LW1hdGVAZnJzLnNvdXJjZWZvcmdlLm5ldAo=" | base64 --decode)
    rsync -rvl -e 'ssh -c arcfour128' --progress ${MODE}/${ARCH}/ ${DEST}:/home/frs/project/u/ub/ubuntu-mate/${VERSION}/${ARCH}/
fi
