#!/usr/bin/env bash

if [ `id -u` != 0 ]; then
    echo "ERROR! You must be root."
    exit 1
fi

ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)

if [ $(basename "${0}") == "precise-graft.sh" ]; then
    CYCLE="12.04.5"
    CODENAME="precise"
    ISO="ubuntu-${CYCLE}-desktop-${ARCH}.iso"
    URL="http://releases.ubuntu.com/precise"
elif [ $(basename "${0}") == "trusty-graft.sh" ]; then
    CYCLE="14.04.2"
    CODENAME="trusty"
    ISO="ubuntu-${CYCLE}-desktop-${ARCH}.iso"
    URL="http://releases.ubuntu.com/trusty"
elif [ $(basename "${0}") == "utopic-graft.sh" ]; then
    CYCLE="14.10"
    CODENAME="utopic"
    ISO="ubuntu-${CYCLE}-desktop-${ARCH}.iso"
    URL="http://releases.ubuntu.com/utopic"
elif [ $(basename "${0}") == "vivid-graft.sh" ]; then
    CYCLE="15.04"
    CODENAME="vivid"
    if [ "${ARCH}" == "powerpc" ]; then
        ISO="lubuntu-${CYCLE}-desktop-${ARCH}.iso"
        URL="http://cdimage.ubuntu.com/lubuntu/releases/14.04/release/"
    else
        ISO="${CODENAME}-desktop-${ARCH}.iso"
        URL="http://cdimage.ubuntu.com/daily-live/current/"
    fi
else
    echo "ERROR! Unknown invocation."
    exit 1
fi

mkdir -p ~/ISO
wget "${URL}/${ISO}" -O ~/ISO/${ISO}
if [ $? -ne 0 ]; then
    echo "ERROR! Download failed!"
    exit 1
fi

umount -f /mnt
mount -o loop ~/ISO/${ISO} /mnt
if [ $? -eq 0 ]; then
    mkdir -p _config/includes.binary.${ARCH}.${CODENAME}
    echo "Updating /boot"
    rsync -a --delete /mnt/boot/ _config/includes.binary.${ARCH}.${CODENAME}/boot/
    if [ "${ARCH}" == "amd64" ]; then
        sed -i 's/ubuntu\.seed/ubuntu-mate\.seed/g' _config/includes.binary.${ARCH}.${CODENAME}/boot/grub/grub.cfg
        sed -i 's/Ubuntu/Ubuntu MATE/g' _config/includes.binary.${ARCH}.${CODENAME}/boot/grub/grub.cfg
    fi
    sed -i 's/ubuntu\.seed/ubuntu-mate\.seed/g' _config/includes.binary.${ARCH}.${CODENAME}/boot/grub/loopback.cfg
    sed -i 's/Ubuntu/Ubuntu MATE/g' _config/includes.binary.${ARCH}.${CODENAME}/boot/grub/loopback.cfg

    echo "Updating /dists"
    rsync -a --delete /mnt/dists/ _config/includes.binary.${ARCH}.${CODENAME}/dists/

    if [ "${ARCH}" == "amd64" ]; then
        echo "Updating /EFI"
        rsync -a --delete /mnt/EFI/ _config/includes.binary.${ARCH}.${CODENAME}/EFI/
    fi

    echo "Updating /install"
    rsync -a --delete /mnt/install/ _config/includes.binary.${ARCH}.${CODENAME}/install/

    echo "Updating /isolinux"
    rsync -a --delete /mnt/isolinux/ _config/includes.binary.${ARCH}.${CODENAME}/isolinux/
    sed -i 's/ubuntu\.seed/ubuntu-mate\.seed/g' _config/includes.binary.${ARCH}.${CODENAME}/isolinux/txt.cfg
    sed -i 's/Ubuntu/Ubuntu MATE/g' _config/includes.binary.${ARCH}.${CODENAME}/isolinux/txt.cfg

    echo "Updating /pool"
    rsync -a --delete /mnt/pool/ _config/includes.binary.${ARCH}.${CODENAME}/pool/

    echo "Updating wubi.exe"
    cp -a /mnt/autorun.inf _config/includes.binary.${ARCH}.${CODENAME}/
    cp -a /mnt/wubi.exe _config/includes.binary.${ARCH}.${CODENAME}/
    rsync -a --delete /mnt/pics/ _config/includes.binary.${ARCH}.${CODENAME}/pics/
    sed -i 's/Ubuntu/Ubuntu MATE/g' _config/includes.binary.${ARCH}.${CODENAME}/autorun.inf

    echo "Rebuilding gfxboot."
    cd _config/includes.binary.${ARCH}.${CODENAME}/isolinux/
    BASE_URL="https://raw.githubusercontent.com/nadrimajstor/syslinux-themes-ubuntu-mate/master"
    wget "${BASE_URL}/ubuntu-mate-syslinux-gfxboot.cfg" -O gfxboot.cfg
    wget "${BASE_URL}/ubuntu-mate-syslinux-splash.png" -O splash.png
    wget "${BASE_URL}/ubuntu-mate-syslinux-splash.pcx" -O splash.pcx
    wget "${BASE_URL}/ubuntu-mate-syslinux-blank.pcx" -O blank.pcx
    wget "${BASE_URL}/ubuntu-mate-syslinux-access.pcx" -O access.pcx
    gfxboot -a bootlogo --add-file gfxboot.cfg splash.png splash.pcx blank.pcx access.pcx
else
    echo "Couldn't mount .iso. All bets are off."
fi
umount /mnt
rm -f ~/ISO/${ISO}
