#!/usr/bin/env bash

# Based on http://people.canonical.com/~laney/random-scripts/build-ubuntu-iso

if [ $(basename "${0}") == "precise-squashfs.sh" ]; then
    CODENAME="precise"
    CYCLE="12.04.5"
    PPAS="ubuntu-mate-dev/ppa ubuntu-mate-dev/precise-mate"
    SYSLINUX_THEME="ubuntu-trusty"
elif [ $(basename "${0}") == "trusty-squashfs.sh" ]; then
    CODENAME="trusty"
    CYCLE="14.04.2"
    PPAS="ubuntu-mate-dev/trusty-mate-hwe-2 ubuntu-mate-dev/ppa ubuntu-mate-dev/trusty-mate accessibility-dev/ppa libreoffice/libreoffice-4-4"
    SYSLINUX_THEME="ubuntu-trusty"
elif [ $(basename "${0}") == "utopic-squashfs.sh" ]; then
    CODENAME="utopic"
    CYCLE="14.10"
    PPAS="ubuntu-mate-dev/ppa"
    SYSLINUX_THEME="ubuntu-trusty"
elif [ $(basename "${0}") == "vivid-squashfs.sh" ]; then
    CODENAME="vivid"
    CYCLE="15.04"
    PPAS="ubuntu-mate-dev/ppa ubuntu-mate-dev/vivid-mate"
    SYSLINUX_THEME="ubuntu-trusty"
else
    echo "ERROR! Unknown invocation."
    exit 1
fi

echo "${CYCLE}" > cycle.txt

if [ $(id -u) = 0 ]; then
    SUDO=env
else
    SUDO=sudo
    ${SUDO} -l
fi

set -e

# Enable the Ubuntu MATE PPA to get access to our modified `livecd-rootfs`.
#${SUDO} apt-add-repository -y ppa:ubuntu-mate-dev/ppa
#${SUDO} apt-get -y update

# Install these packages:
# syslinux syslinux-themes-ubuntu syslinux-themes-ubuntu-trusty
# gfxboot-theme-ubuntu memtest86+ genisoimage livecd-rootfs live-build dpkg-dev
#${SUDO} apt-get -y install dpkg-dev genisoimage gfxboot-theme-ubuntu \
#                           live-build livecd-rootfs lsb-release memtest86+

MIRROR=${MIRROR:-http://archive.ubuntu.com/ubuntu}

ARCH=$(dpkg-architecture -qDEB_HOST_ARCH)

export SUITE=${CODENAME} \
       PROJECT=ubuntu-mate \
       ARCH=$ARCH \
       LB_CHECKSUMS="md5" \
       LB_ARCHITECTURES=$ARCH \
       LB_MIRROR_BOOTSTRAP=$MIRROR \
       LB_HDD_LABEL="UBUNTU-MATE" \
       LB_ISO_APPLICATION="Ubuntu MATE Live" \
       IMAGEFORMAT=squashfs \
       BINARYFORMAT=iso-hybrid \
       LB_SYSLINUX_THEME=${SYSLINUX_THEME} \
       LB_ISO_VOLUME="UbuntuMATE" \
       EXTRA_PPAS="${PPAS}" \
       BUILDER_CODENAME=$(lsb_release -sc)

if [ "${BUILDER_CODENAME}" == "utopic" ]; then
    # Downgrade syslinux to 4.x to avoid breakage with livebuild
    # syslinux-themes-ubuntu syslinux-themes-ubuntu-trusty
    #http://packages.ubuntu.com/search?keywords=syslinux
    #wget -qc "http://cz.archive.ubuntu.com/ubuntu/pool/main/s/syslinux/syslinux-common_4.05+dfsg-6+deb8u1_all.deb" -O /tmp/syslinux-common_4.05+dfsg-6+deb8u1_all.deb
    #wget -qc "http://cz.archive.ubuntu.com/ubuntu/pool/main/s/syslinux/syslinux_4.05+dfsg-6+deb8u1_${ARCH}.deb" -O /tmp/syslinux_4.05+dfsg-6+deb8u1_${ARCH}.deb
    #wget -qc "http://cz.archive.ubuntu.com/ubuntu/pool/main/s/syslinux-themes-ubuntu/syslinux-themes-ubuntu_8_${ARCH}.deb" -O /tmp/syslinux-themes-ubuntu_8_${ARCH}.deb
    #wget -qc "http://cz.archive.ubuntu.com/ubuntu/pool/main/s/syslinux-themes-ubuntu/syslinux-themes-ubuntu-trusty_8_${ARCH}.deb" -O /tmp/syslinux-themes-ubuntu-trusty_8_${ARCH}.deb
    #${SUDO} dpkg -i /tmp/syslinux-common_4.05+dfsg-6+deb8u1_all.deb /tmp/syslinux_4.05+dfsg-6+deb8u1_${ARCH}.deb /tmp/syslinux-themes-ubuntu_8_${ARCH}.deb /tmp/syslinux-themes-ubuntu-trusty_8_${ARCH}.deb
    :
elif [ "${BUILDER_CODENAME}" == "trusty" ]; then
    #${SUDO} apt-get -y install syslinux syslinux-themes-ubuntu syslinux-themes-ubuntu-trusty
    :
else
    echo "ERORR! ${0} doesn't know how to work on ${BUILDER_CODENAME}."
    exit 1
fi

# Inject the livecd-rootfs configuration.
mkdir -p auto
for f in build clean config; do
    cp -f "/usr/share/livecd-rootfs/live-build/auto/$f" auto/
done

${SUDO} lb clean
lb config

sed -i 's/^LB_INITRAMFS_COMPRESSION="lzma"/LB_INITRAMFS_COMPRESSION="gzip"/' config/common

# rename kernel and initrd to what syslinux expects
cat <<EOF > config/hooks/rename-kernel.binary
#!/bin/sh -e
if [ ! -e binary/casper/initrd.lz ]; then
    echo "\$0: Renaming initramfs to initrd.lz..."
    zcat binary/casper/initrd.img-* | lzma -c > binary/casper/initrd.lz
    rm binary/casper/initrd.img-*
fi
if [ ! -e binary/casper/vmlinuz ]; then
    echo "\$0: Renaming kernel to vmlinuz..."
    # This will go wrong if there's ever more than one vmlinuz-* after
    # excluding *.efi.signed.  We can deal with that if and when it arises.
    for x in binary/casper/vmlinuz-*; do
	case \$x in
	    *.efi.signed)
		;;
	    *)
		mv \$x binary/casper/vmlinuz
		if [ -e "\$x.efi.signed" ]; then
		    mv \$x.efi.signed binary/casper/vmlinuz.efi
		fi
		;;
	esac
    done
fi
EOF

# Inject Ubuntu MATE Ubiquity slides
#cp -r _config/includes.chroot/* config/includes.chroot/
#cp -r _config/hooks/* config/hooks/

$SUDO PROJECT=$PROJECT ARCH=$ARCH lb build

# Remove what we do not require.
if [ -d binary/isolinux ]; then
    rm -rf binary/isolinux
fi

# Remove transient data
if [ -d chroot ]; then
    rm -rf chroot
fi

if [ -f binary.hybrid.iso ]; then
    rm -f binary.hybrid.iso
fi

if [ -f binary/MD5SUMS ]; then
    rm -f binary/MD5SUMS
fi

rm -f *.squashfs
